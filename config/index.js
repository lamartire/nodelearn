module.exports = {
  db: 'mongodb://localhost:27017/nodeLearn',
  session: {
    secret: '123qwe123',
    key: 'sid',
    cookie: {
      path: '/',
      maxAge: null,
      httpOnly: true
    }
  }
}
