const UserModel = require('./../models/User');
const express = require('express');
const router = express.Router();
const AuthError = require('./../utils/error').AuthError;
const HttpError = require('./../utils/error').HttpError;

router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

router.get('/auth', (req, res, next) => {
  res.render('auth', { title: 'Auth' });
});

router.post('/auth', (req, res, next) => {
  const { login, password } = req.body;

  return UserModel.auth(login, password)
    .then(user => {
      req.session.user = user._id;
      res.end();
      next();
    })
    .catch(err => {
      return next(err);
    });
});

router.get('/chat', (req, res, next) => {
  console.log('render', req.user)
  res.render('chat', { title: 'Chat' });
});

module.exports = router;
