const express = require('express');
const router = express.Router();
const UserModel = require('./../models/User');
const HttpError = require('./../utils/error').HttpError;
const ObjectId = require('mongodb').ObjectID;

router.get('/', function(req, res, next) {
  UserModel.find({})
    .then(users => {
      res.json(users);
    });
});

router.get('/:id', function(req, res, next) {
  try {
    const id = new ObjectId(req.params.id);
  } catch (err) {
    return next(new HttpError(404, 'User not found!'));
  }

  UserModel.findById(id)
    .then(user => {
      if (!user) {
        next(new HttpError(404, 'User not found!'));
      } else {
        res.json(user);
      }
    })
    .catch(err => {
      next(err);
    })
})

module.exports = router;
