global.Promise = require('bluebird');
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const HttpError = require('./utils/error').HttpError;
const session = require('express-session');
const index = require('./routes/index');
const users = require('./routes/users');
const multiparty = require('connect-multiparty');
const config = require('./config');
const mongoose = require('./connect');
const ejsLayouts = require('express-ejs-layouts');
const app = express();
const MongoStore = require('connect-mongo')(session);
const loadUser = require('./middlewares/loadUser');

// view engine setup
app.engine('ejs', require('ejs-mate'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(multiparty());
app.use(require('./middlewares/sendHttpError'));
app.use(cookieParser());
app.use(session({
  secret: config.session.secret,
  key: config.session.key,
  resave: false,
  saveUninitialized: false,
  cookie: config.session.cookie,
  store: new MongoStore({
    mongooseConnection: mongoose.connection
  })
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use(require('./middlewares/loadUser'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  if (typeof err === 'number') {
    err = new HttpError(err);
  }

  if (err instanceof HttpError) {
    res.sendHttpError(err);
  } else {
    err = new HttpError(500);
    res.sendHttpError(err);
  }
});

module.exports = app;
