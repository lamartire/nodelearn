window.onload = () => {
  const authForm = document.getElementById('auth');

  if (authForm) {
    authForm.onsubmit = (e) => {
      e.preventDefault();
      const { login, password } = e.target.elements;
      const authFormData = {
        login: login.value,
        password: password.value
      }
      let formData = new FormData();

      Object.keys(authFormData).map(key => {
        formData.append(key, authFormData[key]);
      });

      fetch('/auth', {
        method: 'POST',
        body: formData
      })
        .then(res => {
          if (res.status === 200) {
            window.location.href = '/chat';
          } else {
            alert('Авторизация не удалась!');
          }
        })
        .catch(err => {
          if (err) console.error(err);
        })
    }
  }
}
