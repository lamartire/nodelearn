const crypto = require('crypto');
const mongoose = require('./../connect');
const AuthError = require('./../utils/error').AuthError;

const UserSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  salt: {
    type: String,
    required: true
  },
  hashedPassword: {
    type: String,
    required: true
  }
});

UserSchema.method('encryptPassword', function(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
});

UserSchema.method('checkPassword', function(password) {
  return this.encryptPassword(password) === this.hashedPassword;
});

UserSchema.virtual('password')
  .set(function(password) {
    this._plainPassword = password;
    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() {
    return this._plainPassword;
  });

UserSchema.statics.auth = Promise.coroutine(function* (name, password) {
  const UserModel = this;
  let user = yield UserModel.findOne({ name: name });

  if (user) {
    if (user.checkPassword(password)) {
      return user;
    } else {
      throw new AuthError('Пароль неверный!');
    }
  } else {
    const newUser = new UserModel({
      name: name,
      password: password
    });
    user = yield newUser.save();
    return user;
  }
});

const UserModel = mongoose.model('users', UserSchema);

module.exports = UserModel;
