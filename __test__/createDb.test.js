const mongoose = require('./../connect');

function openConnection(mongoose, callback) {
  mongoose.connection.on('open', callback);
}

function dropDatabase(db) {
  return db.dropDatabase();
}

function requireModels() {
  require('./../models/User');

  let modelsPromises = [];

  Object.keys(mongoose.models).map(key => {
    modelsPromises.push(mongoose.models[key].ensureIndexes());
  });

  return Promise.all(modelsPromises);
}

function createUsers(users) {
  let newUsersPromises = [];

  users.map(user => {
    const newUser = new mongoose.models.users({
      name: user,
      password: user + '_password'
    });
    newUsersPromises.push(newUser.save());
  });

  return Promise.all(newUsersPromises);
}

function closeConnection(mongoose) {
  return mongoose.disconnect();
}

openConnection(mongoose, () => {
  const db = mongoose.connection.db;

  dropDatabase(db)
    .then(() => {
      return requireModels();
    })
    .then(() => {
      return createUsers(['Vasya', 'Jim', 'Petya', 'Jann', 'Senya']);
    })
    .then(() => {
      closeConnection(mongoose);
    })
    .catch(err => {
      closeConnection(mongoose);
    })
});
