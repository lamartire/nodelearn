const http = require('http');

class HttpError extends Error {
  constructor(status, message) {
    super();
    this.name = 'HttpError';
    this.status = status;
    this.message = message || http.STATUS_CODES[status] || 'Error';
  }
}

class AuthError extends Error {
  constructor(message) {
    super();
    this.name = 'HttpError';
    this.message = message || 'Error';
  }
}

module.exports = { HttpError, AuthError };
