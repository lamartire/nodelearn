const UserModel = require('./../models/User');

module.exports = (req, res, next) => {
  const { user } = req.session;

  console.log('call middleware', user);

  if (user) {
    return UserModel.findById(user)
      .then(user => {
        req.user = user;
        return next();
      })
      .catch(err => {
        return next(err);
      });
  }

  next();
}
